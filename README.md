## What is this repository for? ##

* A example services repo for API for sampleAPI.
* Demonstrate ability to use Python module for another python app.


## How do I get set up? ##
### For local development/testing ENV ###

* Install Python https://www.python.org/downloads/ (latest) (tested on 3.9.7)
* Install Poetry https://python-poetry.org/docs/#osx--linux--bashonwindows-install-instructions
* TODO

### For production ENV ### 

* TODO

## What to add/improve ###

* unit tests
* extended system requirements specification pyproject.toml https://www.python.org/dev/peps/pep-0518/
* code syntax checking with PyChecker, Pylint, or other (some example overview https://luminousmen.com/post/python-static-analysis-tools)
* project overview

## Used sources/libraries ###

* PyCharm Community Edition https://www.jetbrains.com/pycharm
* Python DOC https://docs.python.org/3.9/
* Poetry for Python https://python-poetry.org/
* Markdown language syntax https://www.markdownguide.org/extended-syntax
* StackOverflow community answers https://stackoverflow.com/search (for some issues)
* Some Youtube videos that describe some tech specs of used language/libraries
* and the most important - BRAIN ;)

### Requirements ###

For system requirements please look in to: 

    https://bitbucket.org/mirekratman/oracle-devops-services/src/master/pyproject.toml

sections [tool.poetry.dependencies], [tool.poetry.dev-dependencies]
