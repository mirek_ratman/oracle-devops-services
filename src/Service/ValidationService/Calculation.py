# Calculation service for data calculation
import math
import operator
import urllib.parse
from typing import Any
from .CalculationException import CalculationValueError, CalculationKeyError

# predefined operators
__operators: Any = {
    '-': operator.sub,
    '/': operator.truediv,
    '*': operator.mul,
    '+': operator.add
}


def calculateValues(pA: float, pB: float, op: str) -> float:
    """
    Process calculation on provided data
    :param pA: float
    :param pB: float
    :param op: str
    :return: float
    """

    try:
        opDecoded = urllib.parse.unquote(op)
        result = float(__operators[opDecoded](float(pA), float(pB)))
        return result
    except KeyError as e:
        raise CalculationKeyError
    except ValueError as e:
        raise CalculationValueError
